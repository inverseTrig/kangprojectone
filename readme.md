This repository contains Project One for Compilers I (CSC 450).

This Java project will use a GUI to read a text file, and print out a word count into another text file.
One class to contain all the functions, one class for the GUI, and one class for the main.