import java.io.*;
import java.util.*;

public class ReadWrite {

	public static void Reader (String dir) throws IOException {
		
		Map<String, Integer> dict = new HashMap<String, Integer>();
		File f = new File(dir);
		
		try {
			Scanner scan = new Scanner(f);
			scan.useDelimiter("[^A-Za-z]+");
			
			while(scan.hasNextLine()) {
				String index = scan.next().toLowerCase();
				if (dict.containsKey(index)) {
					dict.put(index, dict.get(index)+1);
				}
				else {
					dict.put(index, 1);
				}
			}
			System.out.println(dict);
			
			scan.close();
		}
		catch (FileNotFoundException e) {
			System.out.println("Unable to open file '" + dir + "'");
		}
		
		try {
			PrintWriter write = new PrintWriter(new BufferedWriter (new FileWriter(dir+"_stats.txt")));
			
			for (Map.Entry<String, Integer> entry : dict.entrySet()) {
				String key = entry.getKey();
				Integer val = entry.getValue();
				
				write.println(key + " " + val);
			}
			write.close();
		} catch (FileNotFoundException e) {
			System.out.println(dir+ "could not be found.");
		}
		
		
		
	}
	
	
}
